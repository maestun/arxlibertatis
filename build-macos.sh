# brew
curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh

# prerequisites
brew install cmake zlib boost glm freetype openal-soft icnsutil sdl2 libepoxy

# build
mkdir build
cd build
cmake .. -DBUILD_TESTS=OFF
make
